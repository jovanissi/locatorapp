package com.example.locator.activities;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.locator.R;
import com.example.locator.adapters.ServicesAdapter;

import org.json.JSONArray;
import org.json.JSONException;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ServicesActivity extends AppCompatActivity {

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.servicesRecyclerView) RecyclerView servicesRecyclerView;

    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_services);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        context = this;

        String servicesStr = getIntent().getStringExtra("services_string");
        String placeName = getIntent().getStringExtra("place_name");

        this.setTitle(placeName);

        servicesRecyclerView.setHasFixedSize(true);
        servicesRecyclerView.setLayoutManager(new LinearLayoutManager(context));

        try {
            JSONArray servicesArray = new JSONArray(servicesStr);

            servicesRecyclerView.setAdapter(new ServicesAdapter(context, servicesArray));
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
