
package com.example.locator.activities;

import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.example.locator.R;
import com.example.locator.adapters.ModesAdapter;
import com.example.locator.utils.Constants;
import com.example.locator.utils.GoogleDirection;
import com.example.locator.utils.GoogleDirectionsParser;
import com.example.locator.utils.OriginDest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.maps.DirectionsApi;
import com.google.maps.GeoApiContext;
import com.google.maps.android.PolyUtil;
import com.google.maps.errors.ApiException;
import com.google.maps.model.DirectionsResult;
import com.google.maps.model.TravelMode;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.OkHttpClient;

import static com.example.locator.utils.Constants.BASE_URL;

public class DirectionsActivity extends AppCompatActivity implements OnMapReadyCallback,
        GoogleApiClient.OnConnectionFailedListener {

    private Context context;

    @BindView(R.id.loadingView) RelativeLayout loadingView;
    @BindView(R.id.mapView) RelativeLayout mapView;
    @BindView(R.id.modesRecycleView) RecyclerView modesRecyclerView;

    // Variables to be used in the map generation, getting device location, etc
    private FusedLocationProviderClient mFusedLocationProviderClient;
    private Boolean mLocationPermissionsGranted = false;
    private GoogleMap mMap;
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1234;
    private static final float DEFAULT_ZOOM = 15f;
    private double latitude, longitude;

    OriginDest originDest;
    private List<GoogleDirection> googleDirections = new ArrayList<>();
    private ModesAdapter modesAdapter;
    private String place_name = "Destination";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_directions);
        ButterKnife.bind(this);

        context = this;

        modesRecyclerView.setHasFixedSize(true);
        modesRecyclerView.setLayoutManager(new GridLayoutManager(context, 2));

        originDest = (OriginDest) getIntent().getSerializableExtra("originDest");
        place_name = getIntent().getStringExtra("place_name");

        // Construct a FusedLocationProviderClient.
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);

        // Get device location
        getLocationPermission();

        // Initializing AndroidNetworking
        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                .connectTimeout(30, TimeUnit.SECONDS)
                .readTimeout(120, TimeUnit.SECONDS)
                .writeTimeout(120, TimeUnit.SECONDS)
                .retryOnConnectionFailure(false)
                .build();

        AndroidNetworking.initialize(context, okHttpClient);

        getJourney(originDest);

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        // Initializing GoogleMap
        mMap = googleMap;

        mMap.setPadding(0, 50, 0, 0);

        // Checking if the location permissions are granted
        // If not, they are requested to be granted
        if(mLocationPermissionsGranted) {

            // Getting device location
            getDeviceLocation();

            // Request for the location permission to be granted
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this,
                    android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }

            // Update the device location on map
            updateLocationUI();

        }

    }

    // INITIALIZING THE MAP
    private void initMap(){
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);

        mapFragment.getMapAsync(DirectionsActivity.this);
    }

    // REQUESTING PERMISSIONS
    private void getLocationPermission() {
        /*
         * Request location permission, so that we can get the location of the
         * device. The result of the permission request is handled by a callback,
         * onRequestPermissionsResult.
         */
        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionsGranted = true;
            initMap();
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    LOCATION_PERMISSION_REQUEST_CODE);
        }
    }

    // PERMISSION RESULTS
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        mLocationPermissionsGranted = false;

        switch(requestCode){
            case LOCATION_PERMISSION_REQUEST_CODE:{
                if(grantResults.length > 0){
                    for (int grantResult : grantResults) {
                        if (grantResult != PackageManager.PERMISSION_GRANTED) {
                            mLocationPermissionsGranted = false;
                            return;
                        }
                    }
                    mLocationPermissionsGranted = true;

                    //initialize our map
                    initMap();
                }
            }
        }
    }

    // GETTING DEVICE LOCATION
    private void getDeviceLocation() {
        /*
         * Get the best and most recent location of the device, which may be null in rare
         * cases when a location is not available.
         */
        try {
            if (mLocationPermissionsGranted) {
                final Task locationResult = mFusedLocationProviderClient.getLastLocation();
                locationResult.addOnCompleteListener(this, new OnCompleteListener() {
                    @Override
                    public void onComplete(@NonNull Task task) {
                        if (task.isSuccessful()) {
                            // Set the map's camera position to the current location of the device.
                            Location currentLocation = (Location) task.getResult();
                            if (currentLocation != null){
                                latitude = currentLocation.getLatitude();
                                longitude = currentLocation.getLongitude();


                                moveCamera(new LatLng(latitude, longitude), DEFAULT_ZOOM, "My Location");
                            }

                        } else {
                            Toast.makeText(context, "Couldn't get your location. Please check your internet", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        } catch(SecurityException e)  {
            Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    // UPDATE THE DEVICE LOCATION
    private void updateLocationUI() {
        if (mMap == null) {
            return;
        }
        try {
            if (mLocationPermissionsGranted) {
                mMap.setMyLocationEnabled(true);
                mMap.getUiSettings().setMyLocationButtonEnabled(true);
            }
            else {
                mMap.setMyLocationEnabled(false);
                mMap.getUiSettings().setMyLocationButtonEnabled(false);

                getLocationPermission();
            }
        } catch (SecurityException e)  {
            Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    //    MOVING CAMERA
    private void moveCamera(LatLng latLng, float zoom, String title){
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));

    }

    private void setMarkers() {

        mMap.addMarker(new MarkerOptions()
                .draggable(false)
                .position(new LatLng(originDest.getOriginLat(), originDest.getOriginLng()))
                .title("My Location")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));

        mMap.addMarker(new MarkerOptions()
                .draggable(false)
                .position(new LatLng(originDest.getDestinationLat(), originDest.getDestinationLng()))
                .title(place_name)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));

    }

    public void getJourney(OriginDest originDest){

        loadingMode();

        JSONObject params = new JSONObject();
        try {
            params.put("source_long", originDest.getOriginLng());
            params.put("source_lat", originDest.getOriginLat());
            params.put("destination_long", originDest.getDestinationLng());
            params.put("destination_lat", originDest.getDestinationLat());

        } catch (JSONException e) {
            e.printStackTrace();
        }


        AndroidNetworking.post(BASE_URL + "create-journey")
                .setPriority(Priority.MEDIUM)
                .addHeaders("Content-Type", "application/json")
                .addJSONObjectBody(params)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        //GOOGLE DIRECTIONS
                        try {

                            googleDirections = new GoogleDirectionsParser().parse(response.getJSONObject("google"));

                            drawLines(googleDirections.get(0));

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        modesAdapter = new ModesAdapter(context, googleDirections);
                        modesRecyclerView.setAdapter(modesAdapter);

                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });

    }

    private void loadingMode() {
        loadingView.setVisibility(View.VISIBLE);
        mapView.setVisibility(View.GONE);
    }

    private void idleMode() {
        loadingView.setVisibility(View.GONE);
        mapView.setVisibility(View.VISIBLE);
    }

    public void drawLines(GoogleDirection googleDirection){
        List<LatLng> points = new ArrayList<>();

        for (int j = 0; j < googleDirection.getRoutes().get(0).size(); j++) {
            HashMap<String, String> coords = googleDirection.getRoutes().get(0).get(j);
            points.add(new LatLng(Double.parseDouble(coords.get("lat")), Double.parseDouble(coords.get("lng"))));

            PolylineOptions lineOptions = new PolylineOptions();
            lineOptions.width(10);

            lineOptions.addAll(points);
            lineOptions.color(Color.BLUE);

            mMap.addPolyline(lineOptions);
        }

        idleMode();
        setMarkers();

    }

}
