package com.example.locator.activities;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.location.Location;
import android.net.Uri;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.example.locator.R;
import com.example.locator.adapters.PlacesCategoriesAdapter;
import com.example.locator.utils.OriginDest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.maps.android.ui.IconGenerator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.OkHttpClient;

import static com.example.locator.utils.Constants.BASE_URL;

public class MapActivity extends AppCompatActivity implements OnMapReadyCallback,
        GoogleApiClient.OnConnectionFailedListener, GoogleMap.OnMarkerClickListener {

    @BindView(R.id.rootView) RelativeLayout rootView;
    @BindView(R.id.mapView) RelativeLayout mapView;
    @BindView(R.id.placesCatsBtn) LinearLayout placesCatsBtn;

    @BindView(R.id.placesView) LinearLayout placesView;
    @BindView(R.id.backBtn) ImageButton backBtn;
    @BindView(R.id.recyclerView) RecyclerView recyclerView;

    @BindView(R.id.placeCardView) CardView placeCardView;
    @BindView(R.id.placeName) TextView placeName;
    @BindView(R.id.placePhone) TextView placePhone;
    @BindView(R.id.placeComment) TextView placeComment;
    @BindView(R.id.callBtn) LinearLayout callBtn;
    @BindView(R.id.placeDataView) LinearLayout placeDataView;
    @BindView(R.id.loadingPlace) RelativeLayout loadingPlace;
    @BindView(R.id.viewServicesBtn) LinearLayout viewServicesBtn;
    @BindView(R.id.directionsBtn) LinearLayout directionsBtn;

    private Context context;

    // Variables to be used in the map generation, getting device location, etc
    private FusedLocationProviderClient mFusedLocationProviderClient;
    private Boolean mLocationPermissionsGranted = false;
    private GoogleMap mMap;
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1234;
    private static final float DEFAULT_ZOOM = 14f;
    private double myLatitude, myLongitude;

    BottomSheetBehavior sheetBehavior;
    String[] categories;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        ButterKnife.bind(this);

        context = this;

        // Construct a FusedLocationProviderClient.
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);

        // Get device location
        getLocationPermission();

        // Initializing AndroidNetworking
        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                .connectTimeout(30, TimeUnit.SECONDS)
                .readTimeout(120, TimeUnit.SECONDS)
                .writeTimeout(120, TimeUnit.SECONDS)
                .retryOnConnectionFailure(false)
                .build();

        AndroidNetworking.initialize(context, okHttpClient);

        // Places categories recyclerView
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));


        // When the "More" button is connected
        placesCatsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mapView.getVisibility() == View.VISIBLE){
                    getPlacesCategories();
                    mapView.setVisibility(View.GONE);
                    placesView.setVisibility(View.VISIBLE);
                }
            }
        });

        // When "Arrow back button is clicked
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (placesView.getVisibility() == View.VISIBLE){
                    placesView.setVisibility(View.GONE);
                    mapView.setVisibility(View.VISIBLE);
                }
            }
        });

        getPlaces();

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        // Initializing GoogleMap
        mMap = googleMap;

        mMap.setPadding(0, 50, 0, 0);

        mMap.setOnMarkerClickListener(this);

        // Checking if the location permissions are granted
        // If not, they are requested to be granted
        if(mLocationPermissionsGranted) {

            // Getting device location
            getDeviceLocation();

            // Request for the location permission to be granted
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this,
                    android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }

            // Update the device location on map
            updateLocationUI();

        }

    }

    // INITIALIZING THE MAP
    private void initMap(){
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);

        mapFragment.getMapAsync(MapActivity.this);
    }

    // REQUESTING PERMISSIONS
    private void getLocationPermission() {
        /*
         * Request location permission, so that we can get the location of the
         * device. The result of the permission request is handled by a callback,
         * onRequestPermissionsResult.
         */
        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionsGranted = true;
            initMap();
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    LOCATION_PERMISSION_REQUEST_CODE);
        }
    }

    // PERMISSION RESULTS
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        mLocationPermissionsGranted = false;

        switch(requestCode){
            case LOCATION_PERMISSION_REQUEST_CODE:{
                if(grantResults.length > 0){
                    for (int grantResult : grantResults) {
                        if (grantResult != PackageManager.PERMISSION_GRANTED) {
                            mLocationPermissionsGranted = false;
                            return;
                        }
                    }
                    mLocationPermissionsGranted = true;

                    //initialize our map
                    initMap();
                }
            }
        }
    }

    // GETTING DEVICE LOCATION
    private void getDeviceLocation() {
        /*
         * Get the best and most recent location of the device, which may be null in rare
         * cases when a location is not available.
         */
        try {
            if (mLocationPermissionsGranted) {
                final Task locationResult = mFusedLocationProviderClient.getLastLocation();
                locationResult.addOnCompleteListener(this, new OnCompleteListener() {
                    @Override
                    public void onComplete(@NonNull Task task) {
                        if (task.isSuccessful()) {
                            // Set the map's camera position to the current location of the device.
                            Location currentLocation = (Location) task.getResult();
                            if (currentLocation != null){
                                myLatitude = currentLocation.getLatitude();
                                myLongitude = currentLocation.getLongitude();


                                moveCamera(new LatLng(myLatitude, myLongitude), DEFAULT_ZOOM, "My Location");
                            }

                        } else {
                            Toast.makeText(context, "Couldn't get your location. Please check your internet", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        } catch(SecurityException e)  {
            Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    // UPDATE THE DEVICE LOCATION
    private void updateLocationUI() {
        if (mMap == null) {
            return;
        }
        try {
            if (mLocationPermissionsGranted) {
                mMap.setMyLocationEnabled(true);
                mMap.getUiSettings().setMyLocationButtonEnabled(true);
            }
            else {
                mMap.setMyLocationEnabled(false);
                mMap.getUiSettings().setMyLocationButtonEnabled(false);

                getLocationPermission();
            }
        } catch (SecurityException e)  {
            Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    //    MOVING CAMERA
    private void moveCamera(LatLng latLng, float zoom, String title){
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));

    }

    public void getPlacesCategories(){

        final Snackbar snackbar1 = Snackbar.make(rootView, "Loading...", Snackbar.LENGTH_INDEFINITE);
        snackbar1.show();

        AndroidNetworking.get(BASE_URL + "place-categories/")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        snackbar1.dismiss();
                        recyclerView.setAdapter(new PlacesCategoriesAdapter(context, response));
                    }

                    @Override
                    public void onError(ANError anError) {
                        snackbar1.dismiss();

                        final Snackbar snackbar = Snackbar.make(rootView, "Can't reach server. Please check your internet connection", Snackbar.LENGTH_INDEFINITE);
                        snackbar.setAction("RETRY", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                snackbar.dismiss();
                                getPlaces();
                            }
                        });
                        snackbar.show();

                        Log.d("Error", anError.getErrorBody() );
                    }
                });
    }

    public void getPlaces() {

        final Snackbar snackbar1 = Snackbar.make(rootView, "Loading places...", Snackbar.LENGTH_INDEFINITE);
        snackbar1.show();

        AndroidNetworking.get(BASE_URL + "places/")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        snackbar1.dismiss();

                        putPlacesOnMap(response);
                    }

                    @Override
                    public void onError(ANError anError) {
                        snackbar1.dismiss();

                        final Snackbar snackbar = Snackbar.make(rootView, "Can't get places. Please check your internet connection", Snackbar.LENGTH_INDEFINITE);
                        snackbar.setAction("RETRY", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                snackbar.dismiss();
                                getPlaces();
                            }
                        });
                        snackbar.show();

                        Log.d("Error", anError.getErrorBody() );
                    }
                });
    }

    private void putPlacesOnMap(JSONArray jsonArray) {

        for (int i = 0; i < jsonArray.length(); i++) {

            try {
                final JSONObject place = jsonArray.getJSONObject(i);

                LatLng positionCoords = new LatLng(place.getDouble("latitude"), place.getDouble("longitude"));

                mMap.addMarker(new MarkerOptions()
                        .draggable(false)
                        .position(positionCoords)
                        .title(place.getString("name"))
                        .icon(getBitmapFromView(place.getString("name"))));


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    public BitmapDescriptor getBitmapFromView(String title) {
        View view = LayoutInflater.from(context).inflate(R.layout.marker_tooltip, null);
        TextView textView = (TextView) view.findViewById(R.id.tooltips_title);
        textView.setText(title);

        //Get the dimensions of the view. In my case they are in a dimen file
        int width = context.getResources().getDimensionPixelSize(R.dimen.dimen_150dp);
        int height = context.getResources().getDimensionPixelSize(R.dimen.dimen_56dp);

        int measuredWidth = View.MeasureSpec.makeMeasureSpec(width, View.MeasureSpec.EXACTLY);
        int measuredHeight = View.MeasureSpec.makeMeasureSpec(height, View.MeasureSpec.EXACTLY);

        view.measure(measuredWidth, measuredHeight);
        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());

        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);

        view.draw(canvas);

        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    @Override
    public boolean onMarkerClick(Marker marker) {

        placesCatsBtn.setVisibility(View.GONE);
        placeCardView.setVisibility(View.VISIBLE);

        placeDataView.setVisibility(View.GONE);
        loadingPlace.setVisibility(View.VISIBLE);

        LatLng coordinates = marker.getPosition();
        final String name = marker.getTitle();

        final double latitude = coordinates.latitude;
        final double longitude = coordinates.longitude;

        AndroidNetworking.get(BASE_URL + "places?name=" + name + "&latitude=" + latitude + "&longitude=" + longitude)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {


                        try {
                            final JSONObject place = response.getJSONObject(0);

                            placeName.setText(place.getString("name"));
                            placePhone.setText(place.getString("phone"));
                            placeComment.setText(place.getString("comment"));

                            final String phone = place.getString("phone");
                            final String name = place.getString("name");
                            final String servicesStr = place.getJSONArray("services").toString();

                            loadingPlace.setVisibility(View.GONE);
                            placeDataView.setVisibility(View.VISIBLE);

                            callBtn.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent callIntent = new Intent(Intent.ACTION_DIAL);
                                    callIntent.setData(Uri.parse("tel:" + phone));
                                    startActivity(callIntent);
                                }
                            });


                            viewServicesBtn.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(context, ServicesActivity.class);
                                    intent.putExtra("place_name", name);
                                    intent.putExtra("services_string", servicesStr);
                                    startActivity(intent);
                                }
                            });

                            directionsBtn.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    OriginDest originDest = new OriginDest(myLatitude, myLongitude, latitude, longitude);

                                    Intent intent = new Intent(context, DirectionsActivity.class);
                                    intent.putExtra("originDest", originDest);
                                    intent.putExtra("place_name", name);
                                    startActivity(intent);
                                }
                            });


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });

        return false;
    }

    @Override
    public void onBackPressed() {

        if (placeCardView.getVisibility() == View.VISIBLE) {

            placeCardView.setVisibility(View.GONE);
            placesCatsBtn.setVisibility(View.VISIBLE);

        } else {
            super.onBackPressed();
        }

    }
}
