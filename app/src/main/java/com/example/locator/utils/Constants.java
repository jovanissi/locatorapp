package com.example.locator.utils;


public class Constants {

    public static final String BASE_URL = "https://locator-app-api.herokuapp.com/";

    public static final String ROUTES = "routes";
    public static final String WALKING = "walking";
    public static final String TRANSIT = "transit";
    public static final String BICYCLING = "bicycling";
    public static final String DRIVING = "driving";
    public static final String MOTO = "MOTO";
    public static final String UNKNOWN = "unknown";
    public static final String STATUS = "status";
    public static final String OK = "OK";
    public static final String DURATION = "duration";
    public static final String DISTANCE = "distance";
    public static final String LEGS = "legs";
    public static final String ZERO_RESULTS = "ZERO_RESULTS";

}
