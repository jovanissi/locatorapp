package com.example.locator.utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class GoogleDirection {
    private String mode = "";
    private String distance = "unknown";
    private String duration = "unknown";
    private List<List<HashMap<String,String>>> routes = new ArrayList<>();

    public GoogleDirection(String mode, JSONObject resultJSON){
        routes = new DirectionsJSONParser().parse(resultJSON);
        this.mode = mode;

        try{
            String status = resultJSON.getString(Constants.STATUS);

            if (status.equals(Constants.OK)){
                JSONArray routes = resultJSON.getJSONArray(Constants.ROUTES);
                JSONObject firstRouteJSON = routes.getJSONObject(0);

                this.distance = firstRouteJSON.getJSONArray(Constants.LEGS).getJSONObject(0).getJSONObject(Constants.DISTANCE).getString("text");
                this.duration = firstRouteJSON.getJSONArray(Constants.LEGS).getJSONObject(0).getJSONObject(Constants.DURATION).getString("text");
            }

        } catch (JSONException e){
            e.printStackTrace();
        }
    }

    public GoogleDirection(String mode, GoogleDirection googleDirection){
        this.mode = mode;
        this.routes = googleDirection.routes;
        this.duration = googleDirection.duration;
        this.distance = googleDirection.distance;
    }

    public String getMode() {
        return mode;
    }

    public String getDistance() {
        return distance;
    }

    public String getDuration() {
        return duration;
    }

    public List<List<HashMap<String, String>>> getRoutes() {
        return routes;
    }
}
