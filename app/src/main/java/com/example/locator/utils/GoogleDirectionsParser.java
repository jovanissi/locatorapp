package com.example.locator.utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class GoogleDirectionsParser {

    private List<GoogleDirection> googleDirections = new ArrayList<>();

    public GoogleDirectionsParser(){

    }

    public List<GoogleDirection> parse(JSONObject googleJSON){
        try {

            if (!googleJSON.getJSONObject(Constants.TRANSIT).getString(Constants.STATUS).equals(Constants.ZERO_RESULTS)){

                googleDirections.add(new GoogleDirection(Constants.TRANSIT, googleJSON.getJSONObject(Constants.TRANSIT)));
            }

            if (!googleJSON.getJSONObject(Constants.WALKING).getString(Constants.STATUS).equals(Constants.ZERO_RESULTS)){

                googleDirections.add(new GoogleDirection(Constants.WALKING, googleJSON.getJSONObject(Constants.WALKING)));

                if (googleJSON.getJSONObject(Constants.BICYCLING).getString(Constants.STATUS).equals(Constants.ZERO_RESULTS)){

                    googleDirections.add(new GoogleDirection(Constants.BICYCLING, googleJSON.getJSONObject(Constants.WALKING)));
                } else {

                    googleDirections.add(new GoogleDirection(Constants.BICYCLING, googleJSON.getJSONObject(Constants.BICYCLING)));
                }
            }

            if (!googleJSON.getJSONObject(Constants.DRIVING).getString(Constants.STATUS).equals(Constants.ZERO_RESULTS)){

                googleDirections.add(new GoogleDirection(Constants.DRIVING, googleJSON.getJSONObject(Constants.DRIVING)));
                googleDirections.add(new GoogleDirection(Constants.MOTO, googleJSON.getJSONObject(Constants.DRIVING)));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return googleDirections;
    }
}
