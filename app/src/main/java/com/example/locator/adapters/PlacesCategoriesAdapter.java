package com.example.locator.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.locator.R;
import com.example.locator.activities.CategoryMapActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PlacesCategoriesAdapter extends RecyclerView.Adapter<PlacesCategoriesAdapter.ViewHolder> {

    private Context context;
    private JSONArray jsonArray;

    public PlacesCategoriesAdapter(Context context, JSONArray jsonArray) {
        this.context = context;
        this.jsonArray = jsonArray;
    }

    @NonNull
    @Override
    public PlacesCategoriesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.location_types_item, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull PlacesCategoriesAdapter.ViewHolder viewHolder, int i) {

        try {
            final JSONObject jsonObject = jsonArray.getJSONObject(i);

            final String category_id = jsonObject.getString("id");
            final String category_name = jsonObject.getString("category");

            viewHolder.textView.setText(category_name);

            viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, CategoryMapActivity.class);
                    intent.putExtra("category_id", category_id);
                    intent.putExtra("category_name", category_name);
                    context.startActivity(intent);
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return jsonArray.length();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.textView) TextView textView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
