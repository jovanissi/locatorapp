package com.example.locator.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.locator.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ServicesAdapter extends RecyclerView.Adapter<ServicesAdapter.ViewHolder> {

    private Context context;
    private JSONArray jsonArray;

    public ServicesAdapter(Context context, JSONArray jsonArray) {
        this.context = context;
        this.jsonArray = jsonArray;
    }

    @NonNull
    @Override
    public ServicesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.service_item, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ServicesAdapter.ViewHolder viewHolder, int i) {

        try {
            JSONObject jsonObject = jsonArray.getJSONObject(i);

            viewHolder.serviceName.setText(jsonObject.getString("name"));
            viewHolder.serviceProvider.setText(jsonObject.getString("provider"));
            viewHolder.servicePhone.setText(jsonObject.getString("phone"));

            final String phone = jsonObject.getString("phone");

            viewHolder.callBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent callIntent = new Intent(Intent.ACTION_DIAL);
                    callIntent.setData(Uri.parse("tel:" + phone));
                    context.startActivity(callIntent);
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return jsonArray.length();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.serviceName) TextView serviceName;
        @BindView(R.id.serviceProvider) TextView serviceProvider;
        @BindView(R.id.servicePhone) TextView servicePhone;
        @BindView(R.id.callBtn) LinearLayout callBtn;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
