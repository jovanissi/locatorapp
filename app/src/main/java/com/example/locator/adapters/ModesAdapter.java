package com.example.locator.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.locator.R;
import com.example.locator.activities.DirectionsActivity;
import com.example.locator.utils.Constants;
import com.example.locator.utils.GoogleDirection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ModesAdapter extends RecyclerView.Adapter<ModesAdapter.ViewHolder> {

    private Context context;
    private Map<String, Integer> icons = new HashMap<>();
    private List<GoogleDirection> googleDirections = new ArrayList<>();

    public ModesAdapter(Context context, List<GoogleDirection> directions) {
        this.context = context;

        for (int i = 0; i < directions.size(); i++) {
            GoogleDirection dir = directions.get(i);

            if (!dir.getMode().equals(Constants.UNKNOWN) && !dir.getDistance().equals(Constants.UNKNOWN) && !dir.getDuration().equals(Constants.UNKNOWN)){
                this.googleDirections.add(dir);
            }
        }

        icons.put(Constants.WALKING, R.drawable.ic_walk);
        icons.put(Constants.BICYCLING, R.drawable.ic_bike);
        icons.put(Constants.DRIVING, R.drawable.ic_car);
        icons.put(Constants.MOTO, R.drawable.ic_motorcycle);
        icons.put(Constants.TRANSIT, R.drawable.ic_transit);
    }

    @NonNull
    @Override
    public ModesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.modes_item, viewGroup, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ModesAdapter.ViewHolder viewHolder, int i) {
        final GoogleDirection googleDirection = googleDirections.get(i);

        viewHolder.modeIconView.setImageResource(icons.get(googleDirection.getMode()));
        viewHolder.distanceView.setText(googleDirection.getDistance());
        viewHolder.durationView.setText(googleDirection.getDuration());

        if (googleDirection.getMode().equals(Constants.WALKING)){
            viewHolder.costView.setVisibility(View.GONE);
        }

        if (googleDirection.getMode().equals(Constants.MOTO)){
            viewHolder.durationView.setText("<" + googleDirection.getDuration());
        }

        if (googleDirection.getMode().equals(Constants.BICYCLING)){
            viewHolder.durationView.setText("<" + googleDirection.getDuration());
        }

    }

    @Override
    public int getItemCount() {
        return googleDirections.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.modeIcon) ImageView modeIconView;
        @BindView(R.id.distanceView) TextView distanceView;
        @BindView(R.id.estimatedTime) TextView durationView;
        @BindView(R.id.estimatedCost) TextView costView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


        }
    }
}
